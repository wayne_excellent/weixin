package com.ceway.demo.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by CW6472 on 2019/5/5.
 */
@Data
@Component
@ConfigurationProperties(prefix = "wx")
public class WeChatConfig {
    // 微信公众号appid
    private String AppId;
    // 微信公众号AppSecret
    private String AppSecret;
    // 校验token
    private String Token;

}
