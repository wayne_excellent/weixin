package com.ceway.demo.controller;

import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

/**
 * Created by CW6472 on 2019/5/5.
 */
@RestController
@RequestMapping("/weChat")
@Slf4j
public class VerifyController {

    @Autowired
    private WxMpService wxMpService;

    /**
     * 消息校验
     * @param signature
     * @param timestamp
     * @param nonce
     * @param echostr
     * @return
     */
    @GetMapping("/verify")
    public String verify(String signature, String timestamp, String nonce, String echostr) {
        if (!wxMpService.checkSignature(timestamp, nonce, signature)) {
            // 消息不合法
            return "消息不合法";
        }
        log.info("Get消息校验",wxMpService.checkSignature(timestamp, nonce, signature));
        if (StringUtils.isEmpty(echostr))
            return echostr;
        else
            return "";

    }

    /**
     * 消息接受处理
     * @return
     */
    @PostMapping("/verify")
    public String meaasge( HttpServletRequest request ) throws IOException {
        /**
         * 这里为了省事没有去做校验，直接做了处理，开发项目这里必须加上效验
         */
        //获取消息流
        WxMpXmlMessage message = WxMpXmlMessage.fromXml(request.getInputStream());
        //我们可以根据WxMpXmlMessage类中的属性 获取xml中的信息
        //消息类型
        String messageType = message.getMsgType();   //消息类型
        //发送者帐号
        String fromUser = message.getFromUser();
        //开发者微信号
        String touser = message.getToUser();
        //文本消息  文本内容
        String content = message.getContent();

        WxMpXmlOutMessage reMsg = WxMpXmlOutMessage.TEXT()
                .content(content)
                .fromUser(touser)
                .toUser(fromUser)
                .build();
        log.info("消息接受处理",reMsg.toXml());
        if (reMsg != null) {
            // 说明是同步回复的消息
            return reMsg.toXml();
        } else {
            // 说明是异步回复的消息
            return  "";
        }
    }

}
