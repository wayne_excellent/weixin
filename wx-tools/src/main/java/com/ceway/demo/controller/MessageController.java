package com.ceway.demo.controller;

import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.WxMpMassTagMessage;
import me.chanjar.weixin.mp.bean.kefu.WxMpKefuMessage;
import me.chanjar.weixin.mp.bean.result.WxMpMassSendResult;
import me.chanjar.weixin.mp.bean.template.WxMpTemplate;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;

/**
 * Created by CW6472 on 2019/5/5.
 */
@RestController
@RequestMapping("/message")
@Slf4j
public class MessageController {

    @Autowired
    private WxMpService wxMpService;

    /**
     * 客服接口-图文消息
     * @param openid
     * @return
     */
    @RequestMapping("/article")
    public String sendMessage(String openid) {
        WxMpKefuMessage.WxArticle article1 = new WxMpKefuMessage.WxArticle();
        article1.setUrl("https://blog.csdn.net/HELLO_WROLD_/article/details/89472523");
        article1.setPicUrl("http://iacloud.ceway.com.cn/wb/timg%20(1).jpg"
        );
        article1.setDescription("她不再和谁谈论相逢的孤岛" +"因为心里早已荒无人烟");
        article1.setTitle("她她她她");

        WxMpKefuMessage.WxArticle article2 = new WxMpKefuMessage.WxArticle();
        article2.setUrl("https://blog.csdn.net/HELLO_WROLD_/article/details/89472523");
        article2.setPicUrl("http://iacloud.ceway.com.cn/wb/timg%20(1).jpg");
        article2.setDescription("他说你任何为人称道的美丽不及他第一次遇见你");
        article2.setTitle("遇见你");

        WxMpKefuMessage message =  WxMpKefuMessage.NEWS()
                .toUser(openid)
                .addArticle(article1)
//                .addArticle(article2)
                .build();
        // 设置消息的内容等信息
        try {
            log.info("客服接口发送图文消息",message);
            System.out.println(message);
            if (wxMpService.getKefuService().sendKefuMessage(message))
                return "200";
            else
                return "500";
        }catch (WxErrorException e) {
            e.printStackTrace();
            return "500";
        }
    }

    /**
     * 群发文本消息测试接口
     * @return
     */
    @RequestMapping("/GroupToSend")
    public String GroupToSend() {
        try {
            WxMpMassTagMessage massMessage = new WxMpMassTagMessage();
            massMessage.setMsgType(WxConsts.MassMsgType.TEXT);
            massMessage.setContent("测试群发消息\n欢迎欢迎，热烈欢迎\n换行测试\n超链接:<a href=\"http://www.baidu.com\">Hello World</a>");
            massMessage.setSendAll(true);
//            massMessage
//                    .setTagId(wxMpService.getUserTagService().tagGet().get(0).getId());

            WxMpMassSendResult massResult = wxMpService.getMassMessageService()
                    .massGroupMessageSend(massMessage);
            assertNotNull(massResult);
            assertNotNull(massResult.getMsgId());
            log.info("群发接口",massResult);
            return "200";
        } catch (WxErrorException e) {
            e.printStackTrace();
            return "500";
        }


    }

    /**
     * 模板消息测试接口
     * @return
     */
    @RequestMapping("/template")
    public String templateMessage(String openid) {
        List<WxMpTemplate> result = null;
        try {
            result = wxMpService.getTemplateMsgService().getAllPrivateTemplate();
            assertNotNull(result);
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss.SSS");
            WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
                    .toUser(openid)
                    .templateId(result.get(0).getTemplateId())
                    .url("https://blog.csdn.net/HELLO_WROLD_/article/details/89472523")
                    .build();
            templateMessage.addWxMpTemplateData(new WxMpTemplateData("name", dateFormat.format(new Date()), "#FF00FF"));
            templateMessage.addWxMpTemplateData(new WxMpTemplateData("content", dateFormat.format(new Date()), "#FF00FF"));
            String msgId = wxMpService.getTemplateMsgService().sendTemplateMsg(templateMessage);
            assertNotNull(msgId);
            System.out.println(msgId);
            return "200";
        } catch (WxErrorException e) {
            e.printStackTrace();
            return "500";
        }
    }

}
