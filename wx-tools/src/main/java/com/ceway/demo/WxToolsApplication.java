package com.ceway.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WxToolsApplication {

	public static void main(String[] args) {
		SpringApplication.run(WxToolsApplication.class, args);
	}

}
